//<a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/deed.en_GB"><img alt="Creative Commons Licence" style="border-width:0" src="http://i.creativecommons.org/l/by-nc/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/deed.en_GB">Creative Commons Attribution-NonCommercial 4.0 International License</a>.

#ifndef _VOLVO_h
#define _VOLVO_h


#define MY2005

#ifdef MY2005 
typedef enum VOLVO_CAN //change to struct and init on  my05 etc...
{
	
	//Accelerator pedal, analogue input 2 bytes,A6 00 10 00 04 00 01 00,*100/65535
	a6_ambient_pressure = 0x1005,
	a6_batt = 0x1007,
	a6_afr = 0x102A,
	a6_ignition = 0x102C, //BTDC
	a6_tcv = 0x102D,
	a6_injt = 0x1033,
	a6_gear = 0x1046,
	a6_throttle = 0x104E,
	a6_stft = 0x1061,
	a6_opttq = 0x1065,
	a6_egt = 0x1071,
	a6_rpm = 0x1093,
	a6_mph = 0x1094,
	a6_maf = 0x109A,
	a6_rairchg = 0x109F,
	a6_dboost = 0x10A2,
	a6_ait = 0x10AF,
	a6_coolant = 0x10B8,
	a6_boost = 0x10EF,
	a6_knockv1 = 0x1199,
	a6_knockv2 = 0x11E7,
	a6_boostpv = 0x9C,
	a6_PVDKDS = 0x9D, //wrong
	a6_oiltemp = 0x1584 //wrong
};

#endif

#ifdef MY2000 //Should be right but untested.

typedef enum VOLVO_CAN //change to struct and init on  my05 etc...
{
	
	a6_ambient_pressure = 0x1007,
	a6_batt = 0x100A,
	a6_afr = 0x1034,
	a6_ignition = 0x1036, //BTDC
	a6_tcv = 0x1037,
	a6_injt = 0x103D,
	a6_gear = 0x1050,
	a6_throttle = 0x58,
	a6_stft = 0x1070,
	a6_opttq = 0x1076,
	a6_egt = 0x1082,
	a6_rpm = 0x101D,
	a6_mph = 0x1140,
	a6_maf = 0x10AE,
	a6_rairchg = 0x10B5,
	a6_dboost = 0x10B8,
	a6_ait = 0x10CE,
	a6_coolant = 0x10D8,
	a6_boost = 0x129D,
	a6_knockv1 = 0x124A,
	a6_knockv2 = 0x12C3,
	a6_boostpv = 0x9C,
	a6_PVDKDS = 0x9D,
	a6_oiltemp = 0x172B,
};

#endif

#endif
