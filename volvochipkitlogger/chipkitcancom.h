//<a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/deed.en_GB"><img alt="Creative Commons Licence" style="border-width:0" src="http://i.creativecommons.org/l/by-nc/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/deed.en_GB">Creative Commons Attribution-NonCommercial 4.0 International License</a>.

#ifndef _CHIPKITCANCOM_h
#define _CHIPKITCANCOM_h


ME7MSG ME7RXSingleCanCMD(int mod, int channel, uint8_t A6cmd)
{
	// Here we want to send an A6 command and wait for a response, with a timeout.

	ME7MSG rx =  ME7rxCAN(1,1); //module 1 channel 1
	if(rx.data[4] == A6cmd)
	{
		rx.a6_rcvd = true;
		return rx;
	}
	else
	{
		rx.a6_rcvd = false;
	}

rx.a6_rcvd = false; //Should only get here if it's false.....

return rx;

}




void PrintCANResponse(ME7MSG *msg)
{

	ME7Strings str;
	char temp[8];
	ResetBuf();
	char canidtmp[10];
	sprintf(canidtmp, "0x%000000008X",msg->msgID);
	CreateString(canidtmp,true);
        for (int i=0; i<8; i++)
		{ 
          sprintf(temp, "%02X",msg->data[i]); 
		  if(i<7)
		  {
			CreateString(temp,true);
		  }
		  else
		  {
			  CreateString(temp,false);
		  }

	
	  
        }

		

		Serial.println(buf);
		


}

ME7MSG Split29BitCanID(uint32_t canid)
{
	
	ME7MSG message;
	message.SID = (canid >> 18);
	message.EID = canid;
	message.msgID = canid;
	message.PidginID = (canid >> 18) <<18| canid;

	return message;


}

void PrintDiag()

{
  
	ResetBuf();
	CreateString( (ParseFloat(tcv) ),true);
	CreateString( (ParseFloat(maf) ),true);
	CreateString( (ParseFloat(ait) ),true);
	CreateString( (ParseFloat(afr) ),true);
	CreateString( (ParseFloat(ignition) ),true);
	CreateString( (ParseFloat(boost) ),true);
	CreateString( (ParseFloat(throttle) ),true);
	CreateString( (ParseFloat(rpm) ),true);
	CreateString( (ParseFloat(injt) ),true);
	CreateString( (ParseFloat(gear) ),true);
	CreateString( (ParseFloat(opttq) ),true);
	CreateString( (ParseFloat(egt) ),true);
	CreateString( (ParseFloat(mph) ),true);
	CreateString( (ParseFloat(rairchg) ),true);
	CreateString( (ParseFloat(dboost) ),true);
	CreateString( (ParseFloat(batt) ),true);
	CreateString( (ParseFloat(stft) ),true);
	CreateString( (ParseFloat(coolant) ),true);
	CreateString( (ParseFloat(knockv1) ),true);
	CreateString( (ParseFloat(knockv2) ),true);
	CreateString( (ParseFloat(oiltemp) ),true);
	CreateString( (ParseFloat(time) ),false);

#ifdef DEBUG
	Serial.print('\n');
	Serial.print('\r');  
#endif // DEBUG

	Serial.println(buf);
	ResetBuf();

	//return (currentTime + CORE_TICK_RATE * (1000) );
  return;
}



uint32_t D2CANKeepAlive(uint32_t currentTime)

{
  
	//if (!d2cankeepalive_active)
  ME7MSG msg;
  msg.msgID = VOLVO_DIAG;
  msg.data[0] = 0xd8;
  msg.data[1] = 0x00;
  msg.data[2] = 0x00;
  msg.data[3] = 0x00;
  msg.data[4] = 0x00;
  msg.data[5] = 0x00;
  msg.data[6] = 0x00;
  msg.data[7] = 0x00;
 
  // to-do if last update > 10 seconds then close down keepalive.
	
  ME7txCAN(CAN_HS,msg);
  return (currentTime + (CORE_TICK_RATE * 1000) );
  
}

void EndDiagSession()
{

	if (canlogging_on)
	{

		detachCoreTimerService(*D2CANKeepAlive);
		canlogging_on=false;
	}

}

void StartDiagSession()
{

	if (!canlogging_on)
	{
		attachCoreTimerService(D2CANKeepAlive);
		canlogging_on=true;
		logstartmillis = millis();
#ifdef DEBUG
		Serial.println("!!!!!!!!!!!!!!!!!!!!<>!!!!!!!!!!!!!!!!!");  
#endif // DEBUG
	}

	else if(canlogging_on)
	{
		EndDiagSession();
	}
	
}

ME7MSG ME7rxCAN (int mod, int channel) 
 
 {
  
	CAN::RxMessageBuffer * message;
	ME7MSG rx;
	rx.msgRcvd = millis();
	

  if (mod == 1)
  {
	  if (isCAN_HS_MsgReceived == false) 
	  {
		  return rx; ///null
	  }

	  isCAN_HS_MsgReceived = false;
	  message = canMod_HS.getRxMessage(CAN::CHANNEL1);

	canMod_HS.updateChannel(CAN::CHANNEL1);
	canMod_HS.enableChannelEvent(CAN::CHANNEL1, CAN::RX_CHANNEL_NOT_EMPTY, true);
  }	

  else if (mod == 2)
  {
	   if (isCAN_MS_MsgReceived == false) 
		{
		  return rx;
		}

	isCAN_MS_MsgReceived = false;
	message = canMod_MS.getRxMessage(CAN::CHANNEL2);

	canMod_MS.updateChannel(CAN::CHANNEL2);
	canMod_MS.enableChannelEvent(CAN::CHANNEL2, CAN::RX_CHANNEL_NOT_EMPTY, true);
  }
	
	rx.msgID =  (message->msgSID.SID <<18) | message->msgEID.EID;
	rx.EID = (message->msgEID.EID);
	rx.SID = (message->msgSID.SID);
	rx.data[0] = message->data[0];
	rx.data[1] = message->data[1];
	rx.data[2] = message->data[2];
	rx.data[3] = message->data[3];
	rx.data[4] = message->data[4];
	rx.data[5] = message->data[5];
	rx.data[6] = message->data[6];
	rx.data[7] = message->data[7];

	rx.procTime = millis() - rx.msgRcvd; //message processing time in milliseconds
	return rx;
}


void ME7txCAN (int channel, ME7MSG me7) 
{

  CAN::TxMessageBuffer * message;
  if (channel == 1)
  {
    message = canMod_HS.getTxMessageBuffer(CAN::CHANNEL0); // channel 0
  }
  
  else if (channel == 2)
  
  {
    message = canMod_MS.getTxMessageBuffer(CAN::CHANNEL0); //channel 1
  }
 

  if (message != NULL) 
  
  {
    // clear buffer
    message->messageWord[0] = 0;
    message->messageWord[1] = 0;
    message->messageWord[2] = 0;
    message->messageWord[3] = 0;

	message->msgSID.SID = (me7.msgID >> 18);
	message->msgEID.EID = me7.msgID;
    message->msgEID.IDE   = 1;	
    message->msgEID.DLC   = 8;	
    message->data[0]      = me7.data[0];
	message->data[1]      = me7.data[1];
    message->data[2]      = me7.data[2];
    message->data[3]      = me7.data[3];
    message->data[4]      = me7.data[4];
    message->data[5]      = me7.data[5];
    message->data[6]      = me7.data[6];
    message->data[7]      = me7.data[7];


     if(channel == 1)
		{    
			canMod_HS.updateChannel(CAN::CHANNEL0);
			canMod_HS.flushTxChannel(CAN::CHANNEL0);
		}
    
    else if (channel == 2)
    {
      canMod_MS.updateChannel(CAN::CHANNEL0);
      canMod_MS.flushTxChannel(CAN::CHANNEL0);
    }
	
  }	

  else
	  {
		  //if buffer full stop == unplugged or problem
#ifdef DEBUG
		  Serial.println("buffer full...");  
#endif // DEBUG

	  }
}


void initCan_HS(uint32_t myaddr) 
{
  CAN::BIT_CONFIG canBitConfig;
  canMod_HS.enableModule(true);
  canMod_HS.setOperatingMode(CAN::CONFIGURATION);  
  while(canMod_HS.getOperatingMode() != CAN::CONFIGURATION);			
	
  canBitConfig.phaseSeg2Tq            = CAN::BIT_3TQ;
  canBitConfig.phaseSeg1Tq            = CAN::BIT_3TQ;
  canBitConfig.propagationSegTq       = CAN::BIT_3TQ;
  canBitConfig.phaseSeg2TimeSelect    = CAN::TRUE;
  canBitConfig.sample3Time            = CAN::TRUE;
  canBitConfig.syncJumpWidth          = CAN::BIT_2TQ;

  canMod_HS.setSpeed(&canBitConfig,SYS_FREQ,CAN_BUS_SPEED);

  canMod_HS.assignMemoryBuffer(CAN1MessageFifoArea,2 * 8 * 16);	

  canMod_HS.configureChannelForTx(CAN::CHANNEL0,8,CAN::TX_RTR_DISABLED,CAN::LOW_MEDIUM_PRIORITY);
  canMod_HS.configureChannelForRx(CAN::CHANNEL1,8,CAN::RX_FULL_RECEIVE);

  ME7MSG filter = Split29BitCanID(myaddr);

  canMod_HS.configureFilter      (CAN::FILTER1, filter.PidginID, CAN::EID);    
  canMod_HS.configureFilterMask  (CAN::FILTER_MASK1, 0x1FFFFFFF, CAN::EID, CAN::FILTER_MASK_IDE_TYPE); //0x7FF
  canMod_HS.linkFilterToChannel  (CAN::FILTER1, CAN::FILTER_MASK1, CAN::CHANNEL1); 
  canMod_HS.enableFilter         (CAN::FILTER1, true);

  canMod_HS.enableChannelEvent(CAN::CHANNEL1, CAN::RX_CHANNEL_NOT_EMPTY, true);
  canMod_HS.enableModuleEvent(CAN::RX_EVENT, true);

  canMod_HS.setOperatingMode(CAN::NORMAL_OPERATION);
  while(canMod_HS.getOperatingMode() != CAN::NORMAL_OPERATION);	
  
}

void initCan2(uint32_t myaddr) 

{
  CAN::BIT_CONFIG canBitConfig;
  canMod_MS.enableModule(true);
  canMod_MS.setOperatingMode(CAN::CONFIGURATION);
  while(canMod_MS.getOperatingMode() != CAN::CONFIGURATION);			

	
  canBitConfig.phaseSeg2Tq            = CAN::BIT_3TQ;
  canBitConfig.phaseSeg1Tq            = CAN::BIT_3TQ;
  canBitConfig.propagationSegTq       = CAN::BIT_3TQ;
  canBitConfig.phaseSeg2TimeSelect    = CAN::TRUE;
  canBitConfig.sample3Time            = CAN::TRUE;
  canBitConfig.syncJumpWidth          = CAN::BIT_2TQ;

  canMod_MS.setSpeed(&canBitConfig,SYS_FREQ,125000);

  
  canMod_MS.assignMemoryBuffer(CAN2MessageFifoArea,2 * 8 * 16);	

  canMod_MS.configureChannelForTx(CAN::CHANNEL0,8,CAN::TX_RTR_DISABLED,CAN::LOW_MEDIUM_PRIORITY);
  canMod_MS.configureChannelForRx(CAN::CHANNEL1,8,CAN::RX_FULL_RECEIVE);

  canMod_MS.configureFilter      (CAN::FILTER2, myaddr, CAN::EID);    
  canMod_MS.configureFilterMask  (CAN::FILTER_MASK0, 0x1FFFFFFF, CAN::EID, CAN::FILTER_MASK_IDE_TYPE);
  canMod_MS.linkFilterToChannel  (CAN::FILTER0, CAN::FILTER_MASK0, CAN::CHANNEL1); 
  canMod_MS.enableFilter         (CAN::FILTER0, true);

  canMod_MS.enableChannelEvent(CAN::CHANNEL1, CAN::RX_CHANNEL_NOT_EMPTY, true);
  canMod_MS.enableModuleEvent(CAN::RX_EVENT, true);

  canMod_MS.setOperatingMode(CAN::NORMAL_OPERATION);
  while(canMod_MS.getOperatingMode() != CAN::NORMAL_OPERATION);			

}


void doCanHSInterrupt() 
{
  /* This is the CAN1 Interrupt Handler.
   * This is not the actual Interrupt Service Routine,
   * but is the user interrupt handler installed by
   * CAN::attachInterrupt. This is called by the ISR.
   * Note that there are many events in the CAN1 module
   * that can cause this interrupt. These events are 
   * enabled by the CAN::enableModuleEvent() function.
   * In this example, only the CAN::RX_EVENT is enabled. */


  /* Check if the source of the interrupt is CAN::RX_EVENT. 
   * This is redundant  since only this event is enabled
   * in this example but this shows one scheme for handling
   * interrupts. */

  if ((canMod_HS.getModuleEvent() & CAN::RX_EVENT) != 0) {
		
    /* Within this, you can check which event caused the 
     * interrupt by using the CAN::getPendingEventCode() function
     * to get a code representing the highest priority active
     * event.*/ 
		
    if(canMod_HS.getPendingEventCode() == CAN::CHANNEL1_EVENT) {
      /* This means that channel 1 caused the event.
       * The CAN::RX_CHANNEL_NOT_EMPTY event is persistent. You
       * could either read the channel in the ISR
       * to clear the event condition or as done 
       * here, disable the event source, and set
       * an application flag to indicate that a message
       * has been received. The event can be
       * enabled by the application when it has processed
       * one message.
       *
       * Note that leaving the event enabled would
       * cause the CPU to keep executing the ISR since
       * the CAN::RX_CHANNEL_NOT_EMPTY event is persistent (unless
       * the not empty condition is cleared.) 
       * */
			
      canMod_HS.enableChannelEvent(CAN::CHANNEL1, CAN::RX_CHANNEL_NOT_EMPTY, false);
      isCAN_HS_MsgReceived = true;	
    }
  }

  /* The CAN1 Interrupt flag is cleared by the interrupt service routine
   * after this function returns. This will succeed because the event
   * that caused this interrupt to occur (CAN::RX_CHANNEL_NOT_EMPTY) is disabled.
   * The ISR's attempt to clear the CAN1 interrupt flag would fail if the
   * CAN::RX_CHANNEL_NOT_EMPTY event were still enabled because the base event
   * is still present. In this case, another interrupt would occur immediately */ 
	
}

/* ------------------------------------------------------------ */
/***  doCanMSInterrupt
**
**  Parameters:
**      none
**
**  Return Value:
**      none
**
**  Errors:
**      none
**
**  Description:
**      Interrupt service routine to handle interrupt level
**      events for CAN module 2.
*/

void
doCanMSInterrupt() {
  /* This is the CAN2 Interrupt Handler.
   * This is not the actual Interrupt Service Routine,
   * but is the user interrupt handler installd by
   * CAN::attachInterrupt. This is called by the ISR.
   * Note that there are many events in the CAN2 module
   * that can cause this interrupt. These events are 
   * enabled by the CAN::enableModuleEvent() function.
   * In this example, only the CAN::RX_EVENT is enabled. */


  /* Check if the source of the interrupt is CAN::RX_EVENT. 
   * This is redundant  since only this event is enabled
   * in this example but this shows one scheme for handling
   * interrupts. */

  if ((canMod_MS.getModuleEvent() & CAN::RX_EVENT) != 0) {
		
    /* Within this, you can check which event caused the 
     * interrupt by using the CAN::getPendingEventCode() function
     * to get a code representing the highest priority active
     * event.*/ 
		
    if(canMod_MS.getPendingEventCode() == CAN::CHANNEL1_EVENT) {
      /* This means that channel 1 caused the event.
       * The CAN::RX_CHANNEL_NOT_EMPTY event is persistent. You
       * could either read the channel in the ISR
       * to clear the event condition or as done 
       * here, disable the event source, and set
       * an application flag to indicate that a message
       * has been received. The event can be
       * enabled by the application when it has processed
       * one message.
       *
       * Note that leaving the event enabled would
       * cause the CPU to keep executing the ISR since
       * the CAN::RX_CHANNEL_NOT_EMPTY event is persistent (unless
       * the not empty condition is cleared.) 
       * */
			
      canMod_MS.enableChannelEvent(CAN::CHANNEL1, CAN::RX_CHANNEL_NOT_EMPTY, false);
      isCAN_MS_MsgReceived = true;	
    }
  }

  /* The CAN2 Interrupt flag is cleared by the interrupt service routine
   * after this function returns. This will succeed because the event
   * that caused this interrupt to occur (CAN::RX_CHANNEL_NOT_EMPTY) is disabled.
   * The ISR's attempt to clear the CAN2 interrupt flag would fail if the
   * CAN::RX_CHANNEL_NOT_EMPTY event were still enabled because the base event
   * is still present. In this case, another interrupt would occur immediately */	
  
}


#endif

