//<a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/deed.en_GB"><img alt="Creative Commons Licence" style="border-width:0" src="http://i.creativecommons.org/l/by-nc/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/deed.en_GB">Creative Commons Attribution-NonCommercial 4.0 International License</a>.

#ifndef _LOGGING_h
#define _LOGGING_h

float temp;
int update;

int CreateString(char * msg, boolean comma) // 1 = comma, 0 == no
{
	strncpy(buf+strlen(buf), msg, MAXBUF);
	
	if (comma == true)
	{
		strcat(buf, ",");
	}
	
	
	int len = strlen(buf);
	return len;


}

void ResetBuf()
{
	memset(buf, 0, sizeof buf);
}

char *ParseFloat(float msg)
{

	char tmpstr[30];
	sprintf(tmpstr, "%2.2f", msg);
	return tmpstr;
}

void rxLogging(ME7MSG *msg)
{
	
	
	switch (msg->data[4])
	{	
	case a6_tcv:
		temp=ParseResponse(msg, a6_tcv);
			tcv = temp;
			update++;
		break;

	case a6_maf:
	temp=ParseResponse(msg, a6_maf);
	maf = temp;
	update++;
	break;

	case a6_afr:
	temp=ParseResponse(msg, a6_afr);
		afr = temp;
		update++;
	
	break;

	case a6_ignition:
	temp=ParseResponse(msg, a6_ignition);
		ignition = temp;
		update++;
	
	break;

	case a6_boost:
		temp=ParseResponse(msg, a6_boost);
		
		boost = (temp-ambient); 
		if (boost< 5)
		{
			boost = 0;
		}//reference boost.

		
		else
		{
			boost /= 1000; //to BAR
		}
		
		update++;
		break;

	case a6_throttle:
	temp=ParseResponse(msg, a6_throttle);
		throttle = temp;
		update++;
	
	break;

	case a6_rpm:
	temp=ParseResponse(msg, a6_rpm);
		rpm = temp;
		update++;
	break;

	
	case a6_ait:
	temp=ParseResponse(msg, a6_ait);
		ait = temp;
		update++;
	
	break;

	case a6_injt:
	temp=ParseResponse(msg, a6_injt);
	injt = temp;
		update++;
	break;

	
	case a6_gear:
	temp=ParseResponse(msg, a6_gear);
		gear = temp;
		update++;
	break;

	case a6_opttq:
	temp=ParseResponse(msg, a6_opttq);
		opttq = temp;
		update++;
	break;

	
	case a6_egt:
	temp=ParseResponse(msg, a6_egt);
		egt = temp;
		update++;
	break;

	case a6_mph:
	temp=ParseResponse(msg, a6_mph);
		mph = temp;
		update++;
	break;

	
	case a6_rairchg:
	temp=ParseResponse(msg, a6_rairchg);
		rairchg = temp;
		update++;
	break;

	case a6_dboost:
		temp=ParseResponse(msg, a6_dboost);
		
		dboost = (temp-ambient); 
		if (dboost< 5)
		{
			dboost = 0;
		}//reference boost.
			else
		{
			dboost /= 1000; //to BAR
		}
		update++;
	
	break;

	case a6_batt:
		temp=ParseResponse(msg, a6_batt);
		batt = temp;
		update++;
	break;	

	case a6_stft:
		temp=ParseResponse(msg, a6_stft);
		stft = temp;
		update++;
	break;	

	case a6_coolant:
		temp=ParseResponse(msg, a6_coolant);
		coolant = temp;
		update++;
	break;	

	case a6_knockv1:
		temp=ParseResponse(msg, a6_knockv1);
		knockv1 = temp;
		update++;
	break;	

	case a6_knockv2:
		temp=ParseResponse(msg, a6_knockv2);
		knockv2 = temp;
		update++;
	break;	

	case a6_boostpv:
		temp=ParseResponse(msg, a6_boostpv);
		boostpv = temp;
		update++;
	break;	

	case a6_PVDKDS:
		temp=ParseResponse(msg, a6_PVDKDS);
		PVDKDS = temp;
		update++;
	break;	

	case a6_ambient_pressure:
		temp=ParseResponse(msg, a6_ambient_pressure);
		ambient = temp;
		update++;
		break;

	case a6_oiltemp:
		temp=ParseResponse(msg, a6_oiltemp);
		oiltemp = temp;
		update++;
	break;



	}
}


float ParseResponse(ME7MSG * msg, VOLVO_CAN parse)
{

float result;
switch (parse) // ok let's find the type of log to parse
	{

	case a6_boost:	
	result = (msg->data[5] << 8 | msg->data[6]) * 0.0390625;
#ifdef DEBUG
	Serial.print("BOOST!!:  ");
	Serial.println(result);

#endif // DEBUG
	break;
	
	case a6_rpm:
	//
	result = (msg->data[5] << 8 | msg->data[6]) * 0.2500000;
#ifdef DEBUG
	Serial.print("RPM:  ");
	Serial.println(result);

#endif // DEBUG
	break;
	
	case a6_ait:
	//
	result = (msg->data[5] *0.75)-48;
#ifdef DEBUG
	Serial.print("AIT:  ");
	Serial.println(result);  
#endif // DEBUG

	break;

	case a6_maf:	
	result = (msg->data[5] << 8 | msg->data[6]) * 0.1000000;
#ifdef DEBUG
	Serial.print("MAF:  ");
	Serial.println(result);

#endif // DEBUG

	break;
	case a6_afr:
	result = (msg->data[5] << 8 | msg->data[6]) * 0.0002441;
#ifdef DEBUG
	Serial.print("LAMBDA:  ");
	Serial.println(result);

#endif // DEBUG

	break;
	
	case a6_ignition:
	//
	result = msg->data[5] * 0.75;
#ifdef DEBUG
	Serial.print("IGNITION:  ");
	Serial.println(result);

#endif // DEBUG
	break;
	
	case a6_throttle:
	//
	result = msg->data[5] * 0.390625;
#ifdef DEBUG
	Serial.print("THROTTLE:  ");
	Serial.println(result);

#endif // DEBUG

	break;

	case a6_tcv:
	result = msg->data[5] * 0.390625; //(100/256);
#ifdef DEBUG
	Serial.print("TCV:  ");
	Serial.println(result);

#endif // DEBUG
	break;

	case a6_injt:
	//
	 result = (msg->data[5] << 8 | msg->data[6]) * 0.002667; 
#ifdef DEBUG
	Serial.print("Inj Time:  ");
	Serial.println(result);

#endif // DEBUG

	break;

	case a6_gear:
	result = msg->data[5] * 1;
#ifdef DEBUG
	Serial.print("Gear:  ");
	Serial.println(result);

#endif // DEBUG
	break;

	case a6_opttq:
	//
	result = (msg->data[5] << 8 | msg->data[6]) * 0.0015259; 
#ifdef DEBUG
	Serial.print("Opt TQ:  ");
	Serial.println(result);

#endif // DEBUG

	break;

	case a6_egt:
	result = (msg->data[5] << 8 | msg->data[6]) * 0.0195313 - 50; 
#ifdef DEBUG
	Serial.print("EGT:  ");
	Serial.println(result);

#endif // DEBUG
	break;

	case a6_mph:
	result = (msg->data[5] << 8 | msg->data[6]) * 0.0078125;// * 0.621371; //MPH 0078125
	result *= 0.621371; //to mph.
#ifdef DEBUG
	Serial.print("MPH:  ");
	Serial.println(result);

#endif // DEBUG
	break;

	case a6_rairchg:
	result = (msg->data[5] << 8 | msg->data[6]) * 0.0234375;
		
#ifdef DEBUG
	Serial.print("RAIR:  ");
	Serial.println(result);

#endif // DEBUG
	break;

	case a6_dboost:
	//
	result = msg->data[5]* 10;
#ifdef DEBUG
	Serial.print("D_Boost:  ");
	Serial.println(result);

#endif // DEBUG
	break;

	case a6_batt:
	//
	result = (msg->data[5]) * 0.0704;
#ifdef DEBUG
	Serial.print("BATT:  ");
	Serial.println(result);

#endif // DEBUG
	break;
	
	case a6_stft:
	//
	result = (msg->data[5] << 8 | msg->data[6] *2 ) /65535; //broken?
	
#ifdef DEBUG
	Serial.print("STFT:  ");
	Serial.println(result);

#endif // DEBUG
	break;
	
	case a6_coolant:
	//
	result = (msg->data[5]) * 0.75 -48; 
#ifdef DEBUG
	Serial.print("Coolant:  ");
	Serial.println(result);

#endif // DEBUG
	break;
	
	case a6_knockv1:
	//
	result = (msg->data[5] << 8 | msg->data[6]) * 0.0195313;	
#ifdef DEBUG
	Serial.print("KnockV1:  ");
	Serial.println(result);

#endif // DEBUG
	break;
	
	case a6_knockv2:
	//
	result = (msg->data[5] << 8 | msg->data[6]) * 0.0195313;	
#ifdef DEBUG
	Serial.print("KnockV2:  ");
	Serial.println(result);

#endif // DEBUG
	break;
	
	case a6_boostpv:
	//
	result = (msg->data[5] << 8 | msg->data[6]) * 0.0048828125;	
#ifdef DEBUG
	Serial.print("BPS_V:  ");
	Serial.println(result);

#endif // DEBUG
	break;

	case a6_PVDKDS:
	//
	result = msg->data[5] * 10;
#ifdef DEBUG
	Serial.print("PVDKDS?:  ");
	Serial.println(result);

#endif // DEBUG
	break;

	case a6_ambient_pressure:	
	result = msg->data[5] * 5; //we need to read atmosphere
#ifdef DEBUG
	Serial.print("Ambient:  ");
	Serial.println(result);

#endif // DEBUG
	break;

	case a6_oiltemp:
		result = (msg->data[5] *100)/256;
#ifdef DEBUG

	Serial.print("Oil Temp:  ");
	Serial.println(result);
#endif // DEBUG
	
	default:
#ifdef DEBUG
		Serial.println("Parse result not found!");

#endif // DEBUG
		break;
	}
	
	return result;
}



ME7MSG ME7RequestData(uint16_t a6_cmd)// needs tested??
{
	
	uint8_t a6_1;
	uint8_t a6_2;

	a6_1 = (a6_cmd > 8);
	a6_2 = (a6_cmd < 8) > 8; // needs tested??

	ME7MSG msg;
	msg.msgID = VOLVO_DIAG;
	msg.data[0] = 0xCD;
	msg.data[1] = 0x7A;
	msg.data[2] = 0xA6;
	msg.data[3] = a6_1;
	msg.data[4] = a6_2; 
	msg.data[5] = 0x01;
	msg.data[6] = 0x00;
	msg.data[7] = 0x00;
	ME7txCAN(CAN_HS,msg);

	long unsigned timeout;
	int start = millis();

	while (timeout < 20)
	{
	
		timeout++;
		if(isCAN_HS_MsgReceived)
		{
			ME7MSG rxa6 = ME7RXSingleCanCMD(1,1,a6_cmd);
			if(rxa6.a6_rcvd)
			{			
#ifdef DEBUG
				PrintCANResponse(&rxa6);
				Serial.print("Response (ms) in: ");
				int response = (millis() - start);
				Serial.print(response);
				Serial.print(" Retries: ");
				Serial.println(timeout);  
#endif // DEBUG

				return rxa6;
			}
		
			
		}

		else
		{
			
			delay(8);
		}
			
	}
#ifdef DEBUG
	Serial.println("timeout");  
#endif // DEBUG

	//if timeout >3999 //// blaah quit living.

}
#endif

