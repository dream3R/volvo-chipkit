//<a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/deed.en_GB"><img alt="Creative Commons Licence" style="border-width:0" src="http://i.creativecommons.org/l/by-nc/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/deed.en_GB">Creative Commons Attribution-NonCommercial 4.0 International License</a>.
#ifndef _VARS_h
#define _VARS_h

#define CAN_HS 1

float tcv;
float maf;
float afr;
float ignition;
float boost;
float throttle;
float rpm;
float ait;

float injt, gear, opttq, egt, mph, rairchg, dboost;

float batt, stft, coolant, knockv1, knockv2, boostpv, PVDKDS;

float ambient, oiltemp, logstartmillis, time;

unsigned long lastmsg1;
int a6_now =1;

#define MAXBUF 160
char buf[MAXBUF];


// to-do and millis offset by starttime...

#define VOLVO_DIAG 0x000FFFFE //0x000FFFFE

#define ALLOWED_A6 8

boolean printcandata = false;
boolean canlogging_on = false;

typedef enum Responses
{
	R_CMD_OK = 0x1A,
	R_CMD_DENIED = 0x1D,

};


#define SYS_FREQ	(80000000L)
#define CAN_BUS_SPEED   500000		// CAN Speed

CAN    canMod_HS(CAN::CAN1);    // this object uses CAN module 1
CAN    canMod_MS(CAN::CAN2);    // this object uses CAN module 2

/* CAN Message Buffers
*/
uint8_t  CAN1MessageFifoArea[2 * 8 * 16];
uint8_t  CAN2MessageFifoArea[2 * 8 * 16];

/* These are used as event flags by the interrupt service routines.
*/
static volatile bool isCAN_HS_MsgReceived = false;
static volatile bool isCAN_MS_MsgReceived = false;



typedef struct SerialCMD
{
	char cmd[3];
	uint8_t message[8];
	uint32_t canid;
	int len;
	boolean cmd_ok;
	boolean cmd_done;
	int canmodule;
};

typedef struct ME7Strings
{
	char data[8];
	char canid[16];
};

typedef struct ME7MSG
{
	uint8_t     data[8];
	uint32_t    msgID;
	uint32_t	EID;
	uint32_t	SID;
	uint32_t	PidginID;
	unsigned long msgRcvd;
	unsigned long procTime;
	boolean a6_rcvd;


};


/* ------------------------------------------------------------ */
/*		Forward Declarations				*/
/* ------------------------------------------------------------ */

void initCan(uint32_t myaddr);
void initCan2(uint32_t myaddr);
void doCanHSInterrupt();
void doCanMSInterrupt();

void ME7txCAN(int channel, ME7MSG me7);
ME7MSG ME7rxCAN (int mod, int channel);
void StartDiagSession();
float ParseResponse(ME7MSG * msg, VOLVO_CAN parse);
void ME7SetDiagFilters(uint32_t addy, uint32_t mask);
uint32_t D2CANKeepAlive(uint32_t currentTime);
void EndDiagSession();
void PrintDiag();
int CreateString(char * msg, boolean comma);
void ResetBuf();
char *ParseFloat(float msg);
ME7MSG Split29BitCanID(uint32_t canid);
void PrintCANResponse(ME7MSG *msg);
ME7MSG ME7RequestData(uint16_t a6_cmd);
uint32_t ME7ServiceRequestData(uint32_t currentTime);
ME7MSG ME7RXSingleCanCMD(int mod, int channel, uint8_t A6cmd);
void rxLogging(ME7MSG *msg);
SerialCMD ParseSerialCMD(char * cmd);
char ProcessVBCommands(char * msg);
void HandleHostData();  


#endif