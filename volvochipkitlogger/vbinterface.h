//<a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/deed.en_GB"><img alt="Creative Commons Licence" style="border-width:0" src="http://i.creativecommons.org/l/by-nc/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/deed.en_GB">Creative Commons Attribution-NonCommercial 4.0 International License</a>.


// vbinterface.h

#ifndef _VBINTERFACE_h
#define _VBINTERFACE_h

#define MAX_HOST_BUFFER_SIZE 64
char hostBuffer[32];
uint8_t hostBufCount=0;

SerialCMD ParseSerialCMD(char * cmd)
{

	ResetBuf(); //reset buffer
	strncpy(buf, cmd,2); //First two should be the hex command

	SerialCMD tmp;
	strncpy(tmp.cmd, cmd,2);
	tmp.len = strlen(buf); //Size of whole msg passed to function

	if(tmp.len <1)
	{
		return tmp;
	}

	else if (strcmp(buf,"0D")==0) //Start diag logging, end as well
	{
		Serial.print(R_CMD_OK,HEX);
		Serial.print('\r');
		Serial.print('\n');
		tmp.cmd_ok = true;
		return tmp;

	} 

	else
	{
		Serial.println(R_CMD_DENIED,HEX);
	}

	return tmp;

}


char ProcessVBCommands(char * msg)
{

#ifdef DEBUG
	Serial.print("Host:[");
	Serial.print(msg);
	Serial.println("]");
#endif // DEBUG


	SerialCMD command;
	command = ParseSerialCMD(msg);
	
	//Only one command....
	if (strcmp(msg,"0D")==0) //start/stop diag
	{
	StartDiagSession();
	} 

	else if(!command.cmd_done)
		Serial.println("ERR");
}


void HandleHostData()
{
	char readByte=0;
	while ( (Serial.available()!=0) && (hostBufCount<MAX_HOST_BUFFER_SIZE) && (readByte!='\r') )
	{
		readByte = Serial.read();
		if ( (readByte != '\n') && (readByte != '\r') )
			hostBuffer[hostBufCount++] = readByte;
	}
	hostBuffer[hostBufCount]=0;
	if (readByte=='\r')
	{
		ProcessVBCommands(hostBuffer);
		hostBufCount=0;
	}
}

//#endif // DEBUG

#endif

