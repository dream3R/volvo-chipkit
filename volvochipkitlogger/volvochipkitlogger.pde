//Volvo Chipkit Logger for ME7 V0.1a
//2014 John Currie (dream3R) jpcurrie@gmail.com
//Support www.t5d5.org
//
//Note - only MY2005> is supported at present.  K-line interface required for earlier cars.

//Free to use and modify for non-commercial use.
//Base CAN functions were taken from the Dilligent Chipkit sample code, see library.

//<a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/deed.en_GB"><img alt="Creative Commons Licence" style="border-width:0" src="http://i.creativecommons.org/l/by-nc/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/deed.en_GB">Creative Commons Attribution-NonCommercial 4.0 International License</a>.

//I bought a �3.00 OBD2 -> RS232 cable, chipped it and wired the pins to the chipkit as follows:

//4	Chassis Ground //Ground
//5	Signal Ground //Ground
//6	CAN High (J-2284) //Chipkit module 1 canh
//7	ISO 9141-2 K Line and ISO/DIS 14230-4 //not in use currently
//14	CAN Low (J-2284) //Chipkit module 1 canl
//16	Battery power //Chipkit vin careful here!


#include  <WProgram.h>
#include "chipKITCAN.h"

#include "Volvo.h"
#include "vars.h"

#include "chipkitcancom.h"
#include "logging.h"

#include "vbinterface.h"


void setup()
{    
	Serial.begin(115200);
#ifdef DEBUG
	Serial.println("ME7 Logger Alpha v0.1 by dream3R");  
	Serial.print('\n');
#endif // DEBUG

  initCan_HS(0x01200021);
  canMod_HS.attachInterrupt(doCanHSInterrupt);
  attachCoreTimerService(D2CANKeepAlive);
  
}

void loop()
{
	
		 

#ifdef DEBUG
	 unsigned long st=millis();
#endif 
	
	HandleHostData();
	if (canlogging_on)
	{
	
		ME7MSG rx = ME7RequestData(a6_afr);
		rxLogging(&rx);
		rx = ME7RequestData(a6_tcv);
		rxLogging(&rx);
		rx = ME7RequestData(a6_boost);
		rxLogging(&rx);
		rx = ME7RequestData(a6_rpm);
		rxLogging(&rx);
		rx = ME7RequestData(a6_ignition);
		rxLogging(&rx);
		rx = ME7RequestData(a6_throttle);
		rxLogging(&rx);
		rx = ME7RequestData(a6_ait);
		rxLogging(&rx);
		rx = ME7RequestData(a6_injt);
		rxLogging(&rx);
		rx = ME7RequestData(a6_gear);
		rxLogging(&rx);
		rx = ME7RequestData(a6_opttq);
		rxLogging(&rx);
		rx = ME7RequestData(a6_egt);
		rxLogging(&rx);
		rx = ME7RequestData(a6_mph);
		rxLogging(&rx);
		rx = ME7RequestData(a6_rairchg);
		rxLogging(&rx);
		rx = ME7RequestData(a6_dboost);
		rxLogging(&rx);
		rx = ME7RequestData(a6_batt);
		rxLogging(&rx);
		rx = ME7RequestData(a6_stft);
		rxLogging(&rx);
		rx = ME7RequestData(a6_coolant);
		rxLogging(&rx);
		rx = ME7RequestData(a6_knockv1);
		rxLogging(&rx);
		rx = ME7RequestData(a6_knockv2);
		rxLogging(&rx);
		rx = ME7RequestData(a6_oiltemp);
		rxLogging(&rx);
		rx = ME7RequestData(a6_maf);
		rxLogging(&rx);
		rx = ME7RequestData(a6_ambient_pressure);
		rxLogging(&rx);
		time = (millis() - logstartmillis / 1000);
		PrintDiag();

	#ifdef DEBUG
	 unsigned long tm = (millis() - st);
	 Serial.print("latency per msg (ms): ");
	 Serial.println(tm);  
#endif // DEBUG

	}

}

 